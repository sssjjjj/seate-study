package com.seata.samples.business.service;

import com.seata.samples.business.client.OrderClient;
import com.seata.samples.business.client.StorageClient;
import io.seata.core.context.RootContext;
import io.seata.spring.annotation.GlobalTransactional;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @version V1.0
 * @Author 宋聚智
 * @Date 2019/11/6 16:03
 * @description
 * @Modify By：
 * @ModifyTime： 2019/11/6
 * @Modify marker：
 **/
@Service
@Slf4j
public class BusinessService {
    @Autowired
    private StorageClient storageClient;
    @Autowired
    private OrderClient orderClient;
    @GlobalTransactional
    public void purchase(String userId,String commdityCode,int orderCount){
        log.info("purchase begin ..... xid:"+ RootContext.getXID());
        storageClient.deduct(commdityCode,orderCount);
        orderClient.create(userId,commdityCode,orderCount);
    }
    //减库存下单

}
