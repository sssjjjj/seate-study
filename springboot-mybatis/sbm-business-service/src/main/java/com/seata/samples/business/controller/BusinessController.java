package com.seata.samples.business.controller;

import com.seata.samples.business.service.BusinessService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

/**
 * @version V1.0
 * @Author 宋聚智
 * @Date 2019/11/6 16:03
 * @description
 * @Modify By：
 * @ModifyTime： 2019/11/6
 * @Modify marker：
 **/
@RequestMapping("/api/business")
@RestController
public class BusinessController {
    @Autowired
    private BusinessService businessService;
    @RequestMapping("/purchase/commit")
    public Boolean purchasaeCommit(HttpServletRequest request){
        businessService.purchase("1001","2001",1);
        return true;
    }

    @RequestMapping("/purchase/rollback")
    public Boolean purchaseRollback(){
        try{
            businessService.purchase("1002","2001",1);
        }catch (Exception e){
            e.printStackTrace();
            return false;
        }
        return true;
    }
}
