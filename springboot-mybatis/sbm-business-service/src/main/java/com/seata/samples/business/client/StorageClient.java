package com.seata.samples.business.client;

import io.seata.core.context.RootContext;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

/**
 * @version V1.0
 * @Author 宋聚智
 * @Date 2019/11/6 16:02
 * @description
 * @Modify By：
 * @ModifyTime： 2019/11/6
 * @Modify marker：
 **/
@Slf4j
@Component
public class StorageClient {
    @Autowired
    private RestTemplate restTemplate;
    public void deduct(String commdityCode,int orderCount){
        System.out.println("business to storage"+ RootContext.getXID());
        String url = "http://127.0.0.1:8981/api/storage/deduct?commodityCode="+commdityCode+
                "&count="+orderCount;
        try{
            restTemplate.getForEntity(url,Void.class);
        }catch (Exception e){
            log.error("deduct url {},error:",url,e);
            throw new RuntimeException();
        }
    }
}
