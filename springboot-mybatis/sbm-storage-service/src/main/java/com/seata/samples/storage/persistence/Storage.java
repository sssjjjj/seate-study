package com.seata.samples.storage.persistence;

import lombok.Data;

/**
 * @version V1.0
 * @Author 宋聚智
 * @Date 2019/11/6 14:48
 * @description
 * @Modify By：
 * @ModifyTime： 2019/11/6
 * @Modify marker：
 **/
@Data
public class Storage {
    private Integer id;
    private String commodityCode;
    private Integer count;
}
