package com.seata.samples.storage.persistence;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @version V1.0
 * @Author 宋聚智
 * @Date 2019/11/6 14:49
 * @description
 * @Modify By：
 * @ModifyTime： 2019/11/6
 * @Modify marker：
 **/
@Mapper
public interface StorageMapper {
    Storage selectById(@Param("id") Integer id);
    Storage findByCommodityCode(@Param("commodityCode") String commodityCode);
    int updateById(Storage storage);
    void insert(Storage record);
    void insertBatch(List<Storage> records);
    void updateBatch(@Param("list")List<Storage> records,@Param("commodityCode") String commodityCode);
}
