package com.seata.samples.account.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

/**
 * @version V1.0
 * @Author 宋聚智
 * @Date 2019/11/6 14:26
 * @description
 * @Modify By：
 * @ModifyTime： 2019/11/6
 * @Modify marker：
 **/
@Configuration
public class RestTemplateConfig {
    @Bean
    public RestTemplate restTemplate(){
        RestTemplate restTemplate= new RestTemplate();
        return restTemplate;
    }
}
