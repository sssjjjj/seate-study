package com.seata.samples.account;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;

/**
 * @version V1.0
 * @Author 宋聚智
 * @Date 2019/11/6 11:43
 * @description
 * @Modify By：
 * @ModifyTime： 2019/11/6
 * @Modify marker：
 **/
@SpringBootApplication(scanBasePackages="com.seata.samples",exclude = DataSourceAutoConfiguration.class)
public class AccountMybatisApplication {
    public static void main(String[] args){
        SpringApplication.run(AccountMybatisApplication.class,args);
    }
}
