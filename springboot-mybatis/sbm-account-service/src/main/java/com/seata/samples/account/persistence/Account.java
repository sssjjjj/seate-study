package com.seata.samples.account.persistence;

import lombok.Data;

import java.math.BigDecimal;

/**
 * @version V1.0
 * @Author 宋聚智
 * @Date 2019/11/6 14:17
 * @description
 * @Modify By：
 * @ModifyTime： 2019/11/6
 * @Modify marker：
 **/
@Data
public class Account {
    private Integer id;
    private String userId;
    private BigDecimal money;
}
