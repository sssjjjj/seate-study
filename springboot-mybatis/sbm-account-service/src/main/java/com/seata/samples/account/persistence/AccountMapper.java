package com.seata.samples.account.persistence;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

/**
 * @version V1.0
 * @Author 宋聚智
 * @Date 2019/11/6 14:18
 * @description
 * @Modify By：
 * @ModifyTime： 2019/11/6
 * @Modify marker：
 **/
@Mapper
public interface AccountMapper {

    Account  selectByUserId(@Param("userId") String userId);
    int updateById(Account account);
}
