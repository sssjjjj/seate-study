package com.seata.samples.account.controller;

import com.seata.samples.account.service.AccountService;
import io.seata.core.context.RootContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.math.BigDecimal;

/**
 * @version V1.0
 * @Author 宋聚智
 * @Date 2019/11/6 14:27
 * @description
 * @Modify By：
 * @ModifyTime： 2019/11/6
 * @Modify marker：
 **/
@RestController
public class AccontController {
    @Autowired
    AccountService accountService;
    @GetMapping
    public void debit(@RequestParam String userId, @RequestParam BigDecimal orderMoney){
        System.out.println("account XID:{}"+ RootContext.getXID());
        accountService.debit(userId,orderMoney);
    }
}
