package com.seata.simples.common.config;

import com.seata.simples.common.SeataProperties;
import io.seata.spring.annotation.GlobalTransactionScanner;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.util.StringUtils;

/**
 * @version V1.0
 * @Author 宋聚智
 * @Date 2019/11/6 13:04
 * @description
 * @Modify By：
 * @ModifyTime： 2019/11/6
 * @Modify marker：
 **/
@Configuration
@EnableConfigurationProperties({SeataProperties.class})
public class GlobalTransactionAutoConfiguation {
    private final ApplicationContext applicationContext;
    private final SeataProperties seataProperties;

    public GlobalTransactionAutoConfiguation(ApplicationContext applicationContext,SeataProperties seataProperties){
        this.applicationContext=applicationContext;
        this.seataProperties=seataProperties;
    }
    @Bean
    public GlobalTransactionScanner globalTransactionScanner(){
        String applicationName= this.applicationContext.getEnvironment().getProperty("spring.application.name");
        String txServerGroup =seataProperties.getTxServiceGroup();
        if(StringUtils.isEmpty(txServerGroup)){
            txServerGroup=applicationName+"-fascar-service-group";
            this.seataProperties.setTxServiceGroup(txServerGroup);
        }
        return  new GlobalTransactionScanner(applicationName,txServerGroup);
    }
}
