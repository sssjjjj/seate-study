package com.seata.simples.common;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * @version V1.0
 * @Author 宋聚智
 * @Date 2019/11/6 12:58
 * @description
 * @Modify By：
 * @ModifyTime： 2019/11/6
 * @Modify marker：
 **/
@Component
@ConfigurationProperties("spring.cloud.alibaba.seata")
@Getter
@Setter
public class SeataProperties {
    private String txServiceGroup;
    public SeataProperties(){}

}
